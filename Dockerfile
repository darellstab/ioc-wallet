FROM node AS node

WORKDIR /usr/app
COPY ./ /usr/app
COPY .env_demo .env

RUN npm install \
     && npm run build

FROM nginx:latest
COPY --chown=nginx --from=node /usr/app/build/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
