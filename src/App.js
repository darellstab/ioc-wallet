import './App.css';
import {PageRoutes} from "./pages/PageRoutes";
import useLocalStorage from 'react-use-localstorage';

function App() {

    const [token, setToken] = useLocalStorage('ioc-wallet-token', '');
    const [preferences, setPreferences] = useLocalStorage('ioc-wallet-preferences', JSON.stringify({}));

    return (
            <div className="IoCWallet">
                <PageRoutes token={token} setToken={setToken} preferences={JSON.parse(preferences)} setPreferences={preferences => setPreferences(JSON.stringify(preferences))} />
            </div>
    );
}

export default App;
