import React from 'react';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import InsertChartIcon from '@mui/icons-material/InsertChart';
import Container from '@mui/material/Container';

export function AnalyticsList({analytics, removeAnalytics}) {

    return (
            <Container maxWidth="xs">
                <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
                    Web Analytics (session tracking)
                </Typography>
                <List>
                    {analytics.map(item => {
                        return (
                                <ListItem
                                        key={item.id}
                                        secondaryAction={
                                            <IconButton edge="end" aria-label="delete" onClick={() => removeAnalytics(item.id, 'analytics')}>
                                                <DeleteIcon />
                                            </IconButton>
                                        }
                                >
                                    <ListItemAvatar>
                                        <Avatar>
                                            <InsertChartIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                            primary={item.domain + ' (' + item.duration + ' Days)'}
                                    />
                                </ListItem>
                        );
                    })}
                </List>
            </Container>
    );
}
