import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import JSONPretty from 'react-json-pretty';


export function PermissionCard({query, saveHandler, cancelHandler}) {

    return (
        <Card>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Permission request by {query.credentialRequestOrigin}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    {query.credentialRequestOrigin} would like your permission to track you on their site.
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" onClick={saveHandler}>Save</Button>
                <Button size="small" onClick={cancelHandler}>Cancel</Button>
            </CardActions>
        </Card>
    );
}
