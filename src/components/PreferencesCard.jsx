import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import JSONPretty from 'react-json-pretty';


export function PreferencesCard({preferences, saveHandler, cancelHandler}) {

    return (
        <Card>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Settings
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    <b>Credential Type: </b>{preferences.type}<br />
                </Typography>
                <JSONPretty data={preferences.verifiableCredential} />
            </CardContent>
            <CardActions>
                <Button size="small" onClick={saveHandler}>Save</Button>
                <Button size="small" onClick={cancelHandler}>Cancel</Button>
            </CardActions>
        </Card>
    );
}
