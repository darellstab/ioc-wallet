import React from 'react';
import Typography from '@mui/material/Typography';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import FolderIcon from '@mui/icons-material/Folder';
import Container from '@mui/material/Container';

export function TopicsList({topics, removeTopic}) {

    return (
            <Container maxWidth="xs">
                <Typography sx={{ mt: 4, mb: 2 }} variant="h6" component="div">
                    My Topics (personalized marketing)
                </Typography>
                <List>
                    {topics.map(item => {
                        return (
                                <ListItem
                                        key={item.id}
                                        secondaryAction={
                                            <IconButton edge="end" aria-label="delete" onClick={() => removeTopic(item.id, 'topics')}>
                                                <DeleteIcon />
                                            </IconButton>
                                        }
                                >
                                    <ListItemAvatar>
                                        <Avatar>
                                            <FolderIcon />
                                        </Avatar>
                                    </ListItemAvatar>
                                    <ListItemText
                                            primary={item.title}
                                    />
                                </ListItem>
                        );
                    })}
                </List>
            </Container>
    );
}
