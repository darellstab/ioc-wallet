import React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import JSONPretty from 'react-json-pretty';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';


export function VerifiableCredentialAccordion({vc, shareHandler}) {


    return (
        <Accordion>
            <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
            >
                <Typography>{vc.type} : {vc.issuer}</Typography>
                {shareHandler && <Button onClick={() => shareHandler()}>Share</Button>}
            </AccordionSummary>
            <AccordionDetails>
                <JSONPretty data={vc.credentialSubject} />
            </AccordionDetails>
        </Accordion>
    );
}
