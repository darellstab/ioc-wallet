import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import JSONPretty from 'react-json-pretty';


export function VerifiableCredentialCard({vp, saveHandler, cancelHandler}) {

    const vc = Array.isArray(vp.verifiableCredential)
            ? vp.verifiableCredential[0]
            : vp.verifiableCredential;

    function getCredentialType(vc) {
        if(!vc) {
            return 'Credential'
        };
        const types = Array.isArray(vc.type) ? vc.type : [vc.type];
        return types.length > 1 ? types.slice(1).join('/') : types[0];
    }

    return (
        <Card>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Verifiable Credential
                </Typography>
                <Typography variant="body2" color="text.secondary">
                    <b>Credential Type: </b>{getCredentialType(vc)}<br />
                    <b>Issuer: </b>{vc.issuer} <br/>
                    <b>Credential Subject:</b><br />
                </Typography>
                <JSONPretty data={vc.credentialSubject} />
            </CardContent>
            <CardActions>
                <Button size="small" onClick={saveHandler}>Save</Button>
                <Button size="small" onClick={cancelHandler}>Cancel</Button>
            </CardActions>
        </Card>
    );
}
