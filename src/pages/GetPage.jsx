import React, {useEffect, useState} from 'react';
import * as polyfill from 'credential-handler-polyfill';
import {receiveCredentialEvent} from 'web-credential-handler';
import {MEDIATOR} from "../config";
import {createPresentation, fetchCredentials} from "../service/api";
import {VerifiableCredentialAccordion} from "../components/VerifiableCredentialAccordion";
import {createSessionId} from "../service/sessionIdService";

const NO_CONFIRM_CREDENTIALS = ['ThemePreference'];

export function GetPage({preferences}) {

    const [chapiEvent, setChapiEvent] = useState(null);
    const [availableCredentials, setAvailableCredentials] = useState([]);
    const [requestReason, setRequestReason] = useState('');
    const [query, setQuery] = useState(null);

    useEffect(() => {
        polyfill
                .loadOnce(MEDIATOR)
                .then(handleGetEvent);
    }, []);

    useEffect(() => {
        if(query !== null) {
            console.log(query);
            if(NO_CONFIRM_CREDENTIALS.includes(query.credentialQuery.example.type[0])) {
                let preferencesWithAnalytics = preferences;
                if(preferences.hasOwnProperty('analytics')) {

                    const allowedDomain = preferences.analytics.find((item) => item.domain === chapiEvent.credentialRequestOrigin);
                    if(allowedDomain) {
                        preferencesWithAnalytics = {...preferences, analytics: createSessionId(allowedDomain.domain, allowedDomain.duration)}
                    } else {
                        preferencesWithAnalytics = {...preferences, analytics: null}
                    }
                }
                chapiEvent.respondWith(Promise.resolve({dataType: 'PreferencePresentation', data: preferencesWithAnalytics}))
            }
        }
    }, [availableCredentials, query]);

    async function handleGetEvent() {
        const event = await receiveCredentialEvent();

        if (!event) {
            return;
        }

        setChapiEvent(event);

        console.log('Wallet processing get() event:', event);

        const vp = event.credentialRequestOptions.web.VerifiablePresentation;
        const query = Array.isArray(vp.query) ? vp.query[0] : vp.query;
        setQuery(query);

        const type = query.type;
        if(!type === 'QueryByExample') {
            throw new Error('Only QueryByExample requests are supported in demo wallet.');
        }

        const reason = query.credentialQuery.reason;
        const example = query.credentialQuery.example;

        setRequestReason(reason);

        //TODO parse example and pass type to API
        fetchCredentials().then(response => {
            setAvailableCredentials(response);
        });
    }

    function submitCredentials(vc) {
        createPresentation([vc])
                .then(response => {
                    chapiEvent.respondWith(Promise.resolve({dataType: 'VerifiablePresentation', data: response}));
                });
    }

    return (
        <div>
            <h3>Provide Credentials</h3>

            <div>
                Reason: {requestReason}
            </div>

            {chapiEvent && availableCredentials && availableCredentials.map(vc => {
                return (
                        <VerifiableCredentialAccordion vc={vc} key={vc.id} shareHandler={() => submitCredentials(vc)} />
                );
            })}
        </div>
    );
}
