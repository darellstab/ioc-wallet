import React, {useState, useEffect} from 'react';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import {registerWalletWithBrowser} from "../service/chapiService";
import Typography from '@mui/material/Typography';
import {fetchCredentials} from "../service/api";
import {VerifiableCredentialAccordion} from "../components/VerifiableCredentialAccordion";
import {TopicsList} from "../components/TopicsList";
import {AnalyticsList} from "../components/AnalyticsList";


export function ListCredentialsPage({preferences, removePreference}) {

    const [credentials, setCredentials] = useState([]);

    useEffect(() => {
        fetchCredentials().then(response => {
            console.log(response);
            setCredentials(response);
        });
    }, []);

    return (
        <Container maxWidth="md">
            <Card elevation={3}>
                <CardContent>
                    <Typography component="h1" variant="h5">My Credentinals</Typography>

                    {credentials && credentials.map((vc) => {
                        return (
                            <VerifiableCredentialAccordion vc={vc} key={vc.id} />
                        )
                    })}
                </CardContent>
                <CardActions>
                    <Button onClick={() => registerWalletWithBrowser()} size="small">Register Wallet in Browser</Button>
                </CardActions>
            </Card>
            <Grid container spacing={2}>
                <Grid item xs={6}>
                    {preferences.topics &&
                        <TopicsList topics={preferences.topics} removeTopic={removePreference} />
                    }
                </Grid>
                <Grid item xs={6}>
                    {preferences.analytics &&
                        <AnalyticsList analytics={preferences.analytics} removeAnalytics={removePreference} />
                    }
                </Grid>
            </Grid>
        </Container>
    );
}
