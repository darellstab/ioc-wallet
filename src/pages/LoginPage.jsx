import React, {useState} from 'react';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Container from '@mui/material/Container';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import {PasswordInput} from "../components/PasswordInput";
import {authenticate} from "../service/api";

export function LoginPage({setToken}) {

    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');

    function logIn() {
        authenticate(email, password)
                .then(response => {
                    setToken(response.token);
                })
                .catch(error => {
                    // TODO visual feedback
                });
    }

    return (
        <Container component="main" maxWidth="xs">
            <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
            >
                <Typography component="h1" variant="h5">
                    IoC Wallet: Login
                </Typography>

                <Box component="form" onSubmit={logIn} noValidate sx={{ mt: 1 }}>
                    <div>
                        <TextField fullWidth label="E-Mail" value={email} onChange={(event) => setEmail(event.target.value)} />
                    </div>

                    <div>
                        <PasswordInput fullWidth value={password} onChange={(event) => setPassword(event.target.value)} />
                    </div>

                    <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                    >
                        Login as Test-User
                    </Button>
                </Box>
            </Box>
        </Container>
    );
}
