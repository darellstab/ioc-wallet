import React from 'react'
import {Outlet} from 'react-router-dom'
import {LoginPage} from "./LoginPage";
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';


export function PageLayout({token, setToken}) {

    if (token === '') {
        return <LoginPage setToken={setToken}/>;
    }

    return (
            <Container>
                <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'row-reverse',
                        }}
                >
                    <Button onClick={() => setToken('')} variant="text">Logout</Button>
                </Box>
                <main>
                    <Outlet/>
                </main>
            </Container>
    );
}
