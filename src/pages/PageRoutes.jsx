import React from 'react'
import {Route, Routes} from 'react-router-dom';
import {LoginPage} from "./LoginPage";
import {ListCredentialsPage} from "./ListCredentialsPage";
import {PageLayout} from "./PageLayout";
import {NotFoundPage} from "./NotFoundPage";
import {GetPage} from "./GetPage";
import {StorePage} from "./StorePage";
import {WorkerPage} from "./WorkerPage";


export function PageRoutes({token, setToken, preferences, setPreferences}) {

    function removePreference(id, key) {
        const newTopics = preferences[key].filter(item => item.id !== id);

        const newPreferences = {...preferences, [key]: newTopics};

        setPreferences(newPreferences);
    }

    function addTrackableDomain(origin, duration = 7) {
        const newAnalytics = {id: Date.now(), duration: duration, domain: origin};

        let newAnalyticsArray;

        if(Array.isArray(preferences.analytics)) {
            newAnalyticsArray = [...preferences.analytics, newAnalytics];
        } else {
            newAnalyticsArray = [newAnalytics];
        }

        const newPreferences = {...preferences, analytics: newAnalyticsArray};

        setPreferences(newPreferences);
    }

    return (
            <Routes>
                <Route path="login" element={<LoginPage setToken={setToken} />}/>
                <Route path="worker" element={<WorkerPage/>}/>

                <Route path="/" element={<PageLayout token={token} setToken={setToken}/>}>
                    <Route index element={<ListCredentialsPage preferences={preferences} removePreference={removePreference} />}/>
                    <Route path="get" element={<GetPage preferences={preferences} />}/>
                    <Route path="store" element={<StorePage preferences={preferences} setPreferences={setPreferences} addTrackableDomain={addTrackableDomain} />}/>
                    <Route path="*" element={<NotFoundPage/>}/>
                </Route>
            </Routes>
    );
}
