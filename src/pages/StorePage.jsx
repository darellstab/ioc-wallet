import React, {useState, useEffect} from 'react';
import {MEDIATOR} from "../config";
import * as polyfill from 'credential-handler-polyfill';
import {receiveCredentialEvent} from 'web-credential-handler';
import {VerifiableCredentialCard} from "../components/VerifiableCredentialCard";
import {storeCredentials} from "../service/api";
import {PreferencesCard} from "../components/PreferencesCard";
import {PermissionCard} from "../components/PermissionCard";
import {createSessionId} from "../service/sessionIdService";



export function StorePage({preferences, setPreferences, addTrackableDomain}) {

    const [chapiEvent, setChapiEvent] = useState(null);

    useEffect(() => {
        polyfill
                .loadOnce(MEDIATOR)
                .then(handleStoreEvent);
    }, []);

    async function handleStoreEvent() {
        const event = await receiveCredentialEvent();
        setChapiEvent(event);
        console.log(event);
    }

    function saveCredentials() {
        const alias = chapiEvent.credential.dataType + Date.now();

        storeCredentials(alias, chapiEvent.credential.data)
                .then(response => {
                    chapiEvent.respondWith(new Promise(resolve => {
                        return resolve({dataType: chapiEvent.credential.dataType, data: chapiEvent.credential.data})
                    }))
                })
                .catch(error => {
                    // TODO visual feedback
                    console.error(error.message);
                })
    }

    function savePreferences() {
        const receivedPreferences = chapiEvent.credential.data.verifiableCredential;

        setPreferences({...preferences, ...receivedPreferences});
        chapiEvent.respondWith(new Promise(resolve => {
            return resolve({dataType: chapiEvent.credential.dataType, data: receivedPreferences})
        }));
    }

    function cancelStorage() {
        chapiEvent.respondWith(new Promise(resolve => {
            return resolve(null)
        }))
    }

    function allowPermission(origin) {
        addTrackableDomain(origin);
        chapiEvent.respondWith(new Promise(resolve => {
            return resolve({dataType: chapiEvent.credential.dataType, data: {allowed: true, sessionId: createSessionId(origin, 7)}})
        }));
    }

    return (
        <div>
            <h3>Save credentials to wallet</h3>

            {chapiEvent &&
                    <>
                        {chapiEvent.credential.dataType === 'PreferencePresentation' && <PreferencesCard preferences={chapiEvent.credential.data} saveHandler={savePreferences} cancelHandler={cancelStorage} />}
                        {chapiEvent.credential.dataType === 'VerifiablePresentation' && <VerifiableCredentialCard vp={chapiEvent.credential.data} saveHandler={saveCredentials} cancelHandler={cancelStorage} />}
                        {chapiEvent.credential.dataType === 'PermissionPresentation' && <PermissionCard query={chapiEvent} saveHandler={() => allowPermission(chapiEvent.credentialRequestOrigin)} cancelHandler={cancelStorage} />}
                    </>
            }
        </div>
    );
}
