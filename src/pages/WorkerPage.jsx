import React, {useEffect} from 'react';
import {activateHandler} from 'web-credential-handler';
import * as polyfill from 'credential-handler-polyfill';
import {MEDIATOR, WALLET_LOCATION} from "../config";


export function WorkerPage() {

    useEffect(() => {
        console.log('/worker: Activating handler, WALLET_LOCATION:', WALLET_LOCATION);
        activateWalletEventHandler()
                .then(result => console.log('web handler activated'))
    }, []);

    async function activateWalletEventHandler() {
        try {
            await polyfill.loadOnce(MEDIATOR);
        } catch(e) {
            console.error('Error in loadOnce:', e);
        }

        console.log('Worker Polyfill loaded, mediator:', MEDIATOR);

        return activateHandler({
            mediatorOrigin: MEDIATOR,
            async get(event) {
                console.log('WCH: Received get() event:', event);
                return {type: 'redirect', url: WALLET_LOCATION + 'get'};
            },
            async store(event) {
                console.log('WCH: Received store() event:', event);
                return {type: 'redirect', url: WALLET_LOCATION + 'store'};
            }
        })
    }

    return null;
}
