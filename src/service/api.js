
import axios from 'axios';

const baseUrl = process.env.REACT_APP_BACKEND_URL;
const listCredentialsEndpoint = 'credentials/list/';
const storeCredentialsEndpoint = 'credentials/store/';
const createPresentationEndpoint = 'credentials/present/';
const authenticateEndpoint = 'authenticate';

export function fetchCredentials(type = '') {
    return axios.get(baseUrl + listCredentialsEndpoint + type)
            .then((response) => response.data);
}

export function storeCredentials(alias, vc) {
    return axios.put(baseUrl + storeCredentialsEndpoint + alias, {vc: JSON.stringify(vc)});
}

export function createPresentation(vcs) {
    return axios.post(baseUrl + createPresentationEndpoint, {vcs: JSON.stringify(vcs)})
            .then(response => response.data)
}

export function authenticate(username, password) {
    return axios.post(baseUrl + authenticateEndpoint, {username: username, password: password})
            .then(response => response.data)
}
