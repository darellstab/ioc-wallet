import {MEDIATOR, WALLET_LOCATION} from "../config";
import * as polyfill from 'credential-handler-polyfill';
import {installHandler} from 'web-credential-handler';

const workerUrl = WALLET_LOCATION + 'wallet-worker.html';

export async function registerWalletWithBrowser() {
    try {
        await polyfill.loadOnce(MEDIATOR);
    } catch(e) {
        console.error('Error in loadOnce:', e);
    }

    console.log('Polyfill loaded.');

    console.log('Installing wallet worker handler at:', workerUrl);

    // const registration = await installHandler({url: workerUrl});

    // await registration.credentialManager.hints.set(
    //         'test', {
    //             name: 'TestUser',
    //             enabledTypes: ['VerifiablePresentation', 'VerifiableCredential', 'AlumniCredential', 'PreferencePresentation', 'PermissionPresentation']
    //             // enabledTypes: ['VerifiablePresentation']
    //         });

    try {
        await installHandler();
        console.log('Wallet installed.');
    } catch(e) {
        console.error('Wallet installation failed', e);
    }

    console.log('Wallet registered.');
}
