import sha256 from 'crypto-js/sha256';


// this function would normally be implemented in the backend component
export function createSessionId(domain, duration) {

    // this salt would be generated in the backend and used for the specified duration
    const currentSalt = '234lkjv983';

    return sha256(domain + currentSalt).toString();
}
